'use strict'

const {  BrowserWindow } = require('electron');

const defaultProps = {
    width: 1000,
    height: 700,
    show: false,
    icon: `${__dirname}/../renderer/img/logo.ico`
}


class Window extends BrowserWindow{


    constructor({file, ...windowSettings}){
        super({...defaultProps, ...windowSettings});

        this.loadFile(file);
        this.webContents.openDevTools();
        this.setResizable(false)
        // 
        this.once('ready-to-show', () => {
            this.show();
        })
    }
}

module.exports = Window;