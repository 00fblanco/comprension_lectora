var Promise = require('bluebird');
var knex = require('knex')({
    client: 'mysql',
        connection: {
            host : '127.0.0.1',
            user : 'root',
            password : '',
            database : 'comprension_lectora'
        }
    });

function login(req, res){
    const data = req.uploadData[0].json();
    knex.select().from('usuarios')
        .where({
        usuario: data.usuario,
        contrasenia: data.contrasenia // falta aplicar encrypt
        })
    .then(rows => {
        if(rows.length == 0){
            res.json({msg: 'No se encontro usuario ó la contraseña esta mal.', status: 'fail'})
        }else{
            // comparar contraseña con encrypt
            var usuario = rows[0];
            if(usuario.rol == "admin"){
                res.json({...rows[0], contrasenia: '', status: 'ok'});
            }else{
                knex.select().from('personas')
                    .where({ usuario_id: usuario.id })
                    .first()
                    .then(personaRow => {
                        console.log(personaRow)
                        res.json({...personaRow, persona_id: personaRow.id, ...usuario,contrasenia: '', status: 'ok'});
                    })
            }
            
        }
        
    })
}

function crearDocente(req, res){
    const data = req.uploadData[0].json();
    console.log(data);

    knex.transaction((trx) => {
        const { usuario, contrasenia } = data;
        return trx.insert({ usuario, contrasenia })
            .into('usuarios')
            .then((ids) => {
                const { nombre, ap_paterno, ap_materno, telefono, direccion} = data;
                return knex.insert({nombre, ap_paterno, ap_materno, telefono, direccion, usuario_id: ids[0]})
                    .into("personas").transacting(trx);
            })
            .then(trx.commit)
            .catch(trx.rollback);
    }).then((inserts) => {
        res.json({status: 'ok', msg: 'Docente agregado', data: inserts});
    }).catch(error => {
        res.json({ status: 'fail', msg: 'No se pudo crear el docente'});
        console.log(error);
    })
    
}

function getDocentes(req, res){
    knex.select('personas.id as persona_id', 'nombre', 'ap_paterno', 'ap_materno', 'telefono', 'direccion', 'usuario', 'usuario_id').from('personas').innerJoin('usuarios', 'personas.usuario_id', 'usuarios.id')
        .then(rows => {
            res.json({rows, status: 'ok'})
        })
}

function updateDocente(req, res){
    const data = req.uploadData[0].json();
    console.log(data);

    const { persona_id, nombre, ap_paterno, ap_materno, telefono, direccion } = data;

    knex('personas').where({id: persona_id}).update({nombre, ap_paterno, ap_materno, telefono, direccion })
        .then(updateRows => {
            if(updateRows > 0){
                res.json({status: 'ok', msg: 'Registro actualizado'});
            }else{
                res.json({status: 'fail', msg: 'No se pudo actualizar el registro'})
            }
        })
}

function destroyDocente(req, res){
    const data = req.params;
    console.log(data);
    const { id } = data;
    knex('personas').where({id}).del()
        .then(updateRows => {
            console.log(updateRows)
            if(updateRows > 0){
                res.json({status: 'ok', msg: 'Registro eliminado'});
            }else{
                res.json({status: 'fail', msg: 'No se pudo eliminar el registro'})
            }
        })
}


function addPalabra(req, res){
    const data = req.uploadData[0].json();
    console.log(data);
    const { palabra , significado, sinonimo, imagen, usuario_id } = data;
    knex.insert({palabra, significado, sinonimo, imagen, usuario_id }).into('diccionario')
        .then(row => {
            res.json({ status: 'ok', msg: 'Palabra agregada' })
        }).catch(error => {
            console.log(error);
            res.json({ status: 'fail', msg: 'No se pudo guardar la palabra'})
        })
}
function getDiccionario(req, res){
    knex.select().from('diccionario').orderBy('palabra', 'asc')
        .then(rows => {
            rows = rows.map(item => {
                if(item.imagen != null)
                    return {
                        ...item, imagen: Buffer.from(item.imagen, 'base64').toString("utf8")
                    }
                return {...item}
            });
            //console.log(rows);
            res.json({rows, status: 'ok'})
        })
}


function addGrupo(req, res){
    const data = req.uploadData[0].json();
    console.log(data);
    const { materia, grupo, descripcion , usuario_id } = data;
    knex.insert({ materia, grupo, descripcion , docente_id: usuario_id }).into('grupos')
        .then(row => {
            res.json({ status: 'ok', msg: 'Grupo agregado' })
        }).catch(error => {
            console.log(error);
            res.json({ status: 'fail', msg: 'No se pudo agregar el grupo'})
        })
}
function getGrupos(req, res){
    knex.select().from('grupos')
        .where({docente_id: req.params.persona_id})
        .then(rows => {
            res.json({rows, status: 'ok'})
        })
}
function getGrupoAlumnos(req, res){

    knex.select().from('grupo_alumnos')
        .innerJoin('personas', function(){
            this.on('personas.id', '=', 'grupo_alumnos.persona_id')
        })
        .where({grupo_id: req.params.id})
        .then(rows => {
            console.log(rows);
            res.json({rows, status: 'ok'})
        })
}

function destroyGrupo(req, res){
    const data = req.params;
    const { id } = data;
    knex('grupos').where({id}).del()
        .then(updateRows => {
            console.log(updateRows)
            if(updateRows > 0){
                res.json({status: 'ok', msg: 'Registro eliminado'});
            }else{
                res.json({status: 'fail', msg: 'No se pudo eliminar el registro'})
            }
        })
}


function crearAlumno(req, res){
    const data = req.uploadData[0].json();
    knex.transaction((trx) => {
        const { usuario, contrasenia, grupo_id} = data;
        return trx.insert({ usuario, contrasenia, rol: 'alumno' })
            .into('usuarios')
            .then((ids) => {
                const { nombre, ap_paterno, ap_materno, telefono, direccion} = data;
                return knex.insert({nombre, ap_paterno, ap_materno, telefono, direccion, usuario_id: ids[0]})
                    .into("personas").transacting(trx)
                    .then((persona_ids) => {
                        return knex.insert({persona_id: persona_ids[0], grupo_id}).into('grupo_alumnos').transacting(trx)
                    })
            })
            .then(trx.commit)
            .catch(trx.rollback);
    }).then((inserts) => {
        res.json({status: 'ok', msg: 'Alumno agregado al grupo', data: inserts});
    }).catch(error => {
        res.json({ status: 'fail', msg: 'No se pudo crear el docente'});
        console.log(error);
    })
    
}


// LECTURA

function addLectura(req, res){
    const data = req.uploadData[0].json();
    const { titulo, paginas, grupo_id  } = data;
    console.log(data)
    knex.transaction(trx => {
        return trx.insert({titulo, grupo_id})
            .into('lecturas')
            .then(ids => {
                return Promise.map(paginas, (pagina) => {
                    pagina.lectura_id = ids[0];
                    return trx.insert(pagina).into("paginas");
                })
            })
    }).then((inserts) => {
        res.json({status: 'ok', msg: 'Lectura agregada', data: inserts});
    }).catch(error => {
        res.json({ status: 'fail', msg: 'No se pudo crear la lectura'});
        console.log(error);
    })
}

module.exports = { login, crearDocente, getDocentes, updateDocente, destroyDocente,
                    addPalabra, getDiccionario,
                    addGrupo, getGrupos, destroyGrupo, crearAlumno, getGrupoAlumnos,
                    addLectura };
    


