'use strict'
const { Router } = require('electron-routes');
const { app } = require('electron');
const Window = require('./main/window');
// Enable live reload for all the files inside your project directory

require('electron-reload')(__dirname, {
  electron: require(`${__dirname}/../node_modules/electron`)
})


//===================================================
// API
const actions = require('./main/actionsAPI');
let api = new Router('api');

// Valida si el usuario existe en la base de datos
api.post('/login', actions.login);

// DOCENTE
api.post('/docente', actions.crearDocente);
api.put('/docente', actions.updateDocente);
api.delete('/docente/:id', actions.destroyDocente);
api.get('/docente', actions.getDocentes);

// DICCIONARIO
api.get('/diccionario', actions.getDiccionario);
api.post('/diccionario', actions.addPalabra);


// GRUPO
api.post('/grupo', actions.addGrupo);
api.get('/grupo/:persona_id', actions.getGrupos);
api.delete('/grupo/:id', actions.destroyGrupo)

api.get('/grupo/alumnos/:id', actions.getGrupoAlumnos)

// LECTURAS
api.post('add_lectura', actions.addLectura);


// ALUMNOS
api.post('/alumno', actions.crearAlumno);
// END API 
//===================================================
app.on('ready', async() => {
  let mainWindow = new Window({
    file: 'src/renderer/index.html'
  })
});

app.on('window-all-closed', function () {
  if (process.platform != 'darwin') {
    app.quit();
    }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (Window === null) {
    createWindow()
  }
})