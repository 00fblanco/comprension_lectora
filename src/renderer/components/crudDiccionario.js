module.exports = {
    created(){
        this.$store.dispatch('getDiccionario');
    },
    template: `
        <div >
            <vs-card>
                <div slot="header">
                    <h1>Diccionario</h1>
                </div>
            </vs-card>
            <vs-button @click="addPalabra=true" color="primary">Agregar palabra</vs-button>
            <vs-popup  title="Agregar palabra" :active.sync="addPalabra">
                <vs-row class="box-center" vs-justify="center">
                    <vs-input  placeholder="Ingrese la palabra" label="Palabra" v-model="palabra.palabra" class="input-large"/>
                    <vs-textarea label="Significado" v-model="palabra.significado" />
                    <vs-textarea label="Sinonimo" v-model="palabra.sinonimo" />
                    <input type="file" @change="handleFileChange" class="custom-file-input"/>
                    <vs-col>
                        <vs-button @click="agregarPalabra()" color="primary" type="filled" class="space-top" style="margin-left: 90%">Agregar</vs-button>
                    </vs-col>
                </vs-row>
            </vs-popup>
            <vs-row vs-justify="left" class="space-top">
                <vs-col>
                    <vs-list class="box">
                        {{print()}}
                        <div v-for="(palabra, letter) in diccionario">
                            <vs-list-header :title="letter" ></vs-list-header>
                            <div v-for="(subPalabra, index) in palabra">
                            <image :src="subPalabra.imagen"/>
                            </div>
                            <vs-list-item :title="subPalabra.palabra" v-for="(subPalabra, index) in palabra" >
                                <image :src="subPalabra.imagen"/>
                                    <vs-collapse accordion >
                                        <vs-collapse-item class="left">
                                            <h3><strong>Significado: {{subPalabra.significado}}</strong></h3>
                                            <p><strong>Sinonimo: </strong> {{subPalabra.sinonimo}}</p>
                                        </vs-collapse-item>
                                    </vs-collapse>
                            </vs-list-item>
                        </div>
                        
                    </vs-list>
                </vs-col>
            </vs-row>
        </div>
    `,
    data(){
        return {
            addPalabra: false,
            palabra: {
                palabra: '',
                significado: '',
                sinonimo: '',
                imagen: '',
                usuario_id: null,
            }
        }
    },
    methods:{
        print(){
            console.log(this.diccionario)
        },
        handleFileChange(e){
            var file = e.target.files[0];
            var reader  = new FileReader();
            reader.addEventListener('load', () => {
                this.palabra.imagen= reader.result;
            }, false);
            if(file){
                reader.readAsDataURL(file)
            }
        },
        agregarPalabra(){
            // falta validacion
            fetch('api://diccionario', {
                method: 'POST',
                body: JSON.stringify({...this.palabra, usuario_id: this.usuario.id})
            }).then(resp => resp.json())
            .then(data => {
                if(data.status=="fail"){
                    this.$vs.notify({ position: 'top-center', title:'Error',text: data.msg,color:'danger'})
                }else{
                    this.$vs.notify({ position: 'top-center', title: 'Info', text: data.msg, color: 'success'});
                    this.addPalabra = false;
                    this.$store.dispatch('getDiccionario');
                }
            })
        }
        
    },
    computed: {
        usuario(){
            return this.$store.getters.usuario;
        },
        diccionario(){
            var dict = this.$store.getters.diccionario.map(item => {
                return {
                    ...item,
                    letra: item.palabra[0].toUpperCase()
                }
            });

            var grouped = _.mapValues(_.groupBy(dict, 'letra'),
                          clist => clist.map(palabra => _.omit(palabra, 'letra')));
            return grouped;
            
        }

    }
}