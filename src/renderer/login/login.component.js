

module.exports = {    
    template: 
        `
        <vs-row class="box-center" vs-justify="center">
            <vs-col type="flex" vs-justify="center" vs-align="center" vs-w="8">
                <vs-card actionable class="cardx paddings login-box">
                    <div slot="media">
                        <img src="${__dirname+'/../img/logo.png'}" class="image" width="64" />
                    </div>
                    <vs-row class="box-center" vs-justify="center">
                        <vs-col type="flex" vs-justify="center" vs-align="center" vs-w="12">
                            <h1 class="center primary-color"> Inicio de sesión </h1>
                            <h3 class="center primary-color"> Aplicación de comprensión lectora </h3>
                        </vs-col>
                        <div class="space-top space-bottom">
                            <div class="centerx labelx">
                                <vs-input label="Usuario" :danger="error" placeholder="Ingrese su usuario" class="input" v-model="usuario" />
                                <vs-input type="password" :danger="error" label="Ingrese su contraseña" placeholder="Contraseña" class="input" v-model="contrasenia"/>
                            </div>
                            <vs-row vs-justify="flex-end">
                                <vs-button color="primary"  type="border" class="space-top" @click="hola">
                                    Iniciar sesión
                                </vs-button>
                            </vs-row>
                        </div>
                    </vs-row>
                </vs-card>
            </vs-col>
        </vs-row>
        `,
    data(){
        return {
              usuario: 'jorge',
              contrasenia: '123456',
              error: false,
        }
    },
    methods: {
        hola(){
            if(this.usuario.length > 0 && this.contrasenia.length > 0){
                    fetch('api://login', {
                        method: 'post',
                        body: JSON.stringify({
                                usuario: this.usuario,
                                contrasenia: this.contrasenia
                        })
                    })
                    .then(resp => resp.json())
                    .then(data => {
                        if(data.status=="fail"){
                            this.$vs.notify({ position: 'top-center', title:'Error',text: data.msg,color:'danger'})
                        }else{
                            const usuario = data;
                            this.$store.dispatch('login', usuario);
                            switch(usuario.rol){
                                case "admin": 
                                    this.$router.push({ path: '/admin' } );
                                    break;
                                case "maestro":
                                    this.$router.push({ path: '/docente' } );
                                    break;
                                case "alumno":
                                    break;
                            }
                        }
                    });
            }else{
                    this.error=true;
                    this.$vs.notify({ position: 'top-center', title:'Error',text:'Introduce el usuario y contraseña correctamente, no sea mamon ✊🏿',color:'danger'})
            }
        }
    }
}