module.exports = { 
    template: 
        `
        <vs-row class="box-center" vs-justify="center">
            <vs-col type="flex" vs-justify="center" vs-align="left" vs-w="1">
                <div id="parentx">
                    <vs-sidebar  :hidden-background="true" parent="body" :reduce="reduce" :reduce-not-hover-expand="notExpand"  default-index="2"  color="success" class="sidebarx" spacer v-model="active">
                        <div class="header-sidebar" slot="header">
                            <vs-avatar class="center"  size="70px" src="${__dirname+'/../img/logo.png'}"/>
                        </div>
                        <vs-sidebar-group open title="Administrar">
                            <vs-sidebar-item index="2" icon="supervised_user_circle" @click="toMaestros()">
                                Maestros
                            </vs-sidebar-item>
                            <vs-sidebar-item index="3" icon="library_books" @click="toDiccionario()">
                                Diccionario
                            </vs-sidebar-item>
                        </vs-sidebar-group>
                        <div class="footer-sidebar" slot="footer">
                            <vs-button color="danger" type="flat">Salir</vs-button>
                        </div>
                    </vs-sidebar>
                </div>
            </vs-col>
            <vs-col type="flex" vs-justify="center" vs-align="left" vs-w="10">
                <router-view>
                </router-view>
            </vs-col>
            </vs-row>
        `,
    created(){
        this.toMaestros();
    },
    data(){
        return {
            active:true,
            notExpand: false,
            reduce: true
        }
    },
    methods: {
        toMaestros(){
            this.$router.push({path: '/maestros'})
        },
        toDiccionario(){
            this.$router.push({ path: '/diccionario_admin' })
        }
    },
    computed: {
        usuario(){
            return this.$store.getters.usuario;
        }
    }
}