module.exports = {
    created(){
        this.$store.dispatch('getDocentes');
    },
    template: `
        <div >
            <vs-card>
                <div slot="header">
                    <h1>Docentes</h1>
                </div>
            </vs-card>
            <vs-button @click="addDocente=true" color="primary">Agregar maestro</vs-button>
            <vs-popup  title="Agregar docente" :active.sync="addDocente">
                <vs-row class="box-center" vs-justify="center">
                    <vs-input  placeholder="Ingrese el nombre" label="Nombre" v-model="persona.nombre" class="input-large"/>
                    <vs-input  placeholder="Ingrese el apellido paterno" label="Apellido paterno" v-model="persona.ap_paterno" class="input-large"/>
                    <vs-input  placeholder="Ingrese el apellido materno" label="Apellido materno" v-model="persona.ap_materno" class="input-large"/>

                    <vs-input placeholder="Ingrese el telefono" label="Telefono" v-model="persona.telefono" class="input-large"/>

                    <vs-input placeholder="Ingrese la direccion" label="Dirección" v-model="persona.direccion" class="input-large"/>
                    <vs-divider> Datos de usuario</vs-divider>

                    <vs-input placeholder="Ingrese su usuario" label="Usuario" v-model="persona.usuario" class="input-large"/>
                    <vs-input type="password" placeholder="Ingrese su contraseña"  label="Contraseña" v-model="persona.contrasenia" class="input-large"/>
                    <vs-col>
                        <vs-button @click="agregarDocente()" color="primary" type="filled" class="space-top" style="margin-left: 83%">Agregar</vs-button>
                    </vs-col>
                </vs-row>
            </vs-popup>
            <vs-row vs-justify="left" class="space-top">
                <vs-col>
                    <vs-table stripe :data="docentes">
                        <template slot="header">
                            <h3>
                                Docentes
                            </h3>
                        </template>
                        <template slot="thead">
                            <vs-th>Usuario</vs-th>
                            <vs-th>Nombre</vs-th>
                            <vs-th>Apellido Paterno</vs-th>
                            <vs-th>Apellido Materno</vs-th>
                            <vs-th>Telefono</vs-th>
                            <vs-th>Dirección</vs-th>
                            <vs-th>Acción</vs-th>
                        </template>
                        <template slot-scope="{data}">
                            <vs-tr :key="indextr" v-for="(tr, indextr) in data">
                                <vs-td :data="tr.usuario">
                                    {{tr.usuario}}
                                </vs-td>
                                <vs-td :data="tr.nombre">
                                    {{tr.nombre}}
                                    <template slot="edit">
                                        <vs-input v-model="tr.nombre" @blur="update(tr)" class="inputx" placeholder="Nombre"/>
                                    </template>
                                </vs-td>
                                <vs-td :data="tr.ap_paterno">
                                    {{tr.ap_paterno}}
                                    <template slot="edit">
                                        <vs-input v-model="tr.ap_paterno" @blur="update(tr)" class="inputx" placeholder="Apellido paterno"/>
                                    </template>
                                </vs-td>
                                <vs-td :data="tr.ap_materno">
                                    {{tr.ap_materno}}
                                    <template slot="edit">
                                        <vs-input v-model="tr.ap_materno" @blur="update(tr)" class="inputx" placeholder="Apellido materno"/>
                                    </template>
                                </vs-td>
                                <vs-td :data="tr.telefono">
                                    {{tr.telefono}}
                                    <template slot="edit">
                                        <vs-input v-model="tr.telefono" @blur="update(tr)" class="inputx" placeholder="Telefono"/>
                                    </template>
                                </vs-td>
                                <vs-td :data="tr.direccion">
                                    {{tr.direccion}}
                                    <template slot="edit">
                                        <vs-input v-model="tr.direccion" @blur="update(tr)" class="inputx" placeholder="Direccion"/>
                                    </template>
                                </vs-td>
                                <vs-td>
                                    <vs-button vs-type="flat" size="small" color="danger" icon="delete_sweep" @click="eliminarDocente(tr)"></vs-button>
                                </vs-td>
                            </vs-tr>
                        </template>
                    </vs-table>
                </vs-col>
            </vs-row>
        </div>
    `,
    data(){
        return {
            addDocente: false,
            persona:{
                nombre: '',
                ap_paterno: '',
                ap_materno: '',
                telefono: '',
                direccion: '',
                usuario: '',
                contrasenia: ''
            },
        }
    },
    methods:{
        update(row){
            fetch('api://docente', {method: 'put', body: JSON.stringify(row)})
                .then(data => {
                    if(data.status=="fail"){
                        this.$vs.notify({ position: 'top-center', title:'Error',text: data.msg,color:'danger'})
                    }
                })
        },
        agregarDocente(){
            // falta validacion
            fetch('api://docente', {
                method: 'POST',
                body: JSON.stringify(this.persona)
            }).then(resp => resp.json())
            .then(data => {
                if(data.status=="fail"){
                    this.$vs.notify({ position: 'top-center', title:'Error',text: data.msg,color:'danger'})
                }else{
                    this.$vs.notify({ position: 'top-center', title: 'Info', text: data.msg, color: 'success'});
                    this.$store.dispatch('getDocentes');
                    this.addDocente = false;
                }
            })
        },
        eliminarDocente(row){
            this.$store.dispatch('destroyDocente', row.persona_id);
        }
    },
    computed: {
        docentes(){
            return this.$store.getters.docentes;
        }
    }
}