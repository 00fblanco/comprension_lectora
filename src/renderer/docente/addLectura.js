module.exports = {
    template: `
        <div>
            <vs-card>
                <div slot="header">
                    <h1>Agregar lectura</h1>
                </div>
                    <vs-row class="box-center" vs-justify="center">
                    <vs-alert title="Erores" active="true" color="danger" v-if="errores != ''">
                        {{this.errores}}
                    </vs-alert>
                        <vs-input  placeholder="Titulo de la lectura" label="Titulo" v-model="lectura.titulo" class="input"/>

                        <vs-col>
                            <vs-button @click="agregarPagina()" type="line" icon="add">Agregar pagina</vs-button>
                        </vs-col>
                        <vs-textarea class="space-top" height="150px" counter="500" label="Pagina de la lectura" :counter-danger.sync="counterDanger" v-model="lectura.paginas[paginaActual-1].contenido" />
                    <vs-pagination :total="lectura.paginas.length" v-model="paginaActual"></vs-pagination>
                    
                    <vs-col>
                        <vs-button @click="agregarLectura()" color="primary" type="filled" class="space-top" style="margin-left: 83%">Agregar</vs-button>
                    </vs-col>
                </vs-row>
            </vs-card>
        </div>
    `,
    data(){
        return {
            lectura: {
                titulo: '',
                paginas: [{contenido: ''}]
            },
            paginaActual: 1,
            counterDanger: false,
            errores: '',
        }
    },
    methods:{
        toGrupos(){
            this.$router.push({ path: '/docente' })
        },
        agregarLectura(){
            try{
                if(this.lectura.titulo.length < 3)
                    throw "*El titulo es obligatorio"
                this.lectura.paginas.forEach(pag => {
                    if(pag.length === 0){
                        throw "*Hay paginas vacias, favor de llenarlas"
                    }
                })
                this.errores = '';

                fetch('api://add_lectura', {
                    method: 'POST',
                    body: JSON.stringify({...this.lectura, grupo_id: this.$route.params.grupo_id})
                }).then(resp => resp.json())
                .then(data => {
                    if(data.status=="fail"){
                        this.$vs.notify({ position: 'top-center', title:'Error',text: data.msg,color:'danger'})
                    }else{
                        this.$vs.notify({ position: 'top-center', title: 'Info', text: data.msg, color: 'success'});
                        this.toGrupos();
                    }
                })
            }catch(err){
                console.log(err);
                this.errores = err;
            }
        },
        agregarPagina(){
            this.lectura.paginas.push({contenido: ''});
        }
    },
    computed:{
    }

}