module.exports = {
    created(){
        this.$store.dispatch('getGrupos', this.usuario.persona_id);
        //console.log(this.usuario)
    },
    template: `
        <div >
            <vs-card>
                <div slot="header">
                    <h1>Grupos</h1>
                </div>
            </vs-card>
            <vs-button @click="addGrupo=true" color="primary">Agregar grupo</vs-button>
            <vs-popup title="Alumnos en el grupo" :active.sync="verAlumnosPop">
                <vs-table stripe :data="grupo_alumnos">
                    <template slot="header">
                        <h3>
                            Alumnos
                        </h3>
                    </template>
                    <template slot="thead">
                        <vs-th>Nombre</vs-th>
                        <vs-th>Ap. Paterno</vs-th>
                        <vs-th>Ap. Materno</vs-th>
                        <vs-th>Telefono</vs-th>
                        <vs-th>Dirección</vs-th>
                    </template>
                    <template slot-scope="{data}">
                        <vs-tr :key="indextr" v-for="(tr, indextr) in data">
                            <vs-td :data="tr.nombre">
                                {{tr.nombre}}
                            </vs-td>
                            <vs-td :data="tr.nombre">
                                {{tr.ap_paterno}}
                            </vs-td>
                            <vs-td :data="tr.nombre">
                                {{tr.ap_materno}}
                            </vs-td>
                            <vs-td :data="tr.nombre">
                                {{tr.telefono}}
                            </vs-td>
                            <vs-td :data="tr.nombre">
                                {{tr.direccion}}
                            </vs-td>
                        </vs-tr>
                    </template>
                </vs-table>
            </vs-popup>
            <vs-popup  title="Agregar grupo" :active.sync="addGrupo">
                <vs-row class="box-center" vs-justify="center">
                    <vs-input  placeholder="Ingrese el grupo ej. 3-A" label="Grupo: " v-model="grupo.grupo" class="input-large"/>
                    <vs-input  placeholder="Ingrese la materia ej. Matematicas" label="Materia: " v-model="grupo.materia" class="input-large"/>
                    <vs-input  placeholder="Ingrese descripción" label="Descripción" v-model="grupo.descripcion" class="input-large"/>

                    <vs-col>
                        <vs-button @click="agregarGrupo()" color="primary" type="filled" class="space-top" style="margin-left: 83%">Agregar</vs-button>
                    </vs-col>
                </vs-row>
            </vs-popup>
            <vs-popup  title="Agregar alumno" :active.sync="addAlumno">
                <vs-row class="box-center" vs-justify="center">
                <vs-input  placeholder="Ingrese el nombre" label="Nombre" v-model="persona.nombre" class="input-large"/>
                <vs-input  placeholder="Ingrese el apellido paterno" label="Apellido paterno" v-model="persona.ap_paterno" class="input-large"/>
                <vs-input  placeholder="Ingrese el apellido materno" label="Apellido materno" v-model="persona.ap_materno" class="input-large"/>

                <vs-input placeholder="Ingrese el telefono" label="Telefono" v-model="persona.telefono" class="input-large"/>

                <vs-input placeholder="Ingrese la direccion" label="Dirección" v-model="persona.direccion" class="input-large"/>
                <vs-divider> Datos de usuario</vs-divider>

                <vs-input placeholder="Ingrese su usuario" label="Usuario" v-model="persona.usuario" class="input-large"/>
                <vs-input type="password" placeholder="Ingrese su contraseña"  label="Contraseña" v-model="persona.contrasenia" class="input-large"/>
                <vs-col>
                    <vs-button @click="agregarAlumno()" color="primary" type="filled" class="space-top" style="margin-left: 83%">Agregar</vs-button>
                </vs-col>
                </vs-row>
            </vs-popup>
            <vs-row vs-justify="left" class="space-top">
                <vs-col>
                    <vs-table stripe :data="grupos">
                        <template slot="header">
                            <h3>
                                Grupos
                            </h3>
                        </template>
                        <template slot="thead">
                            <vs-th>Grupo</vs-th>
                            <vs-th>Materia</vs-th>
                            <vs-th>Descripción</vs-th>
                            <vs-th>Alumnos</vs-th>
                            <vs-th>Lecturas</vs-th>
                            <vs-th>Acciones</vs-th>
                        </template>
                        <template slot-scope="{data}">
                            <vs-tr :key="indextr" v-for="(tr, indextr) in data">
                                <vs-td :data="tr.grupo">
                                    {{tr.grupo}}
                                </vs-td>
                                <vs-td :data="tr.materia">
                                    {{tr.materia}}
                                </vs-td>
                                <vs-td :data="tr.descripcion">
                                    {{tr.descripcion}}
                                </vs-td>
                                <vs-td>
                                    <vs-button vs-type="flat" size="small" @click="verAlumnos(tr)">Ver</vs-button>
                                    <vs-button vs-type="flat" size="small" @click="addAlumno=true;grupo_id=tr.id">Agregar</vs-button>
                                </vs-td>
                                <vs-td>
                                    <vs-button vs-type="flat" size="small" @click="verLecturas(tr)">Ver</vs-button>
                                    <vs-button vs-type="flat" size="small" @click="toAddLectura(tr.id)">Agregar</vs-button>
                                </vs-td>
                                <vs-td>
                                    <vs-button vs-type="flat" size="small" color="danger" icon="delete_sweep" @click="eliminarGrupo(tr)"></vs-button>
                                </vs-td>
                            </vs-tr>
                        </template>
                    </vs-table>
                </vs-col>
            </vs-row>
        </div>
    `,
    data(){
        return {
            addGrupo: false,
            addAlumno: false,
            verAlumnosPop: false,
            grupo_id: 0,
            grupo: {
                materia: '',
                grupo: '',
                descripcion: '',
            },
            persona:{
                nombre: '',
                ap_paterno: '',
                ap_materno: '',
                telefono: '',
                direccion: '',
                usuario: '',
                contrasenia: ''
            },
        }
    },
    methods:{
        agregarGrupo(){
            // falta validacion
            fetch('api://grupo', {
                method: 'POST',
                body: JSON.stringify({...this.grupo, usuario_id: this.usuario.persona_id})
            }).then(resp => resp.json())
            .then(data => {
                if(data.status=="fail"){
                    this.$vs.notify({ position: 'top-center', title:'Error',text: data.msg,color:'danger'})
                }else{
                    this.$vs.notify({ position: 'top-center', title: 'Info', text: data.msg, color: 'success'});
                    this.$store.dispatch('getGrupos', this.usuario.persona_id);
                    this.addGrupo = false;
                }
            })
        },
        eliminarGrupo(row){
            this.$store.dispatch('destroyGrupo', row.id);
            this.$store.dispatch('getGrupos', this.usuario.persona_id);
        },
        agregarAlumno(){
            // falta validacion
            fetch('api://alumno', {
                method: 'POST',
                body: JSON.stringify({...this.persona, grupo_id: this.grupo_id})
            }).then(resp => resp.json())
            .then(data => {
                if(data.status=="fail"){
                    this.$vs.notify({ position: 'top-center', title:'Error',text: data.msg,color:'danger'})
                }else{
                    this.$vs.notify({ position: 'top-center', title: 'Info', text: data.msg, color: 'success'});
                    this.addAlumno = false;
                }
            })
        },
        verAlumnos(tr){
            this.$store.dispatch('getGrupoAlumnos',tr.id);
            this.verAlumnosPop = true;
        },
        verLecturas(tr){

        },
        toAddLectura(grupo_id){
            this.$router.push({ path: '/add_lectura/'+grupo_id } )
        }
    },
    computed: {
        grupos(){
            return this.$store.getters.grupos;
        },
        usuario(){
            return this.$store.getters.usuario;
        },
        grupo_alumnos(){
            return this.$store.getters.grupo_alumnos;
        }
    }
}