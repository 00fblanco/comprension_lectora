var { rendererPreload } = require('electron-routes');
rendererPreload();
const store = require('./store');

// ADMINS COMPONENTS
const Admin = require('./admin/admin.js');
const Login = require('./login/login.component.js');
const crudDocentes = require('./admin/crudDocentes.js');
const crudDiccionario = require('./components/crudDiccionario.js');


// DOCENTE COMPONENTS
const Docente = require('./docente/index.js');
const crudGrupos = require('./docente/crudGrupos.js');
const addLectura = require('./docente/addLectura.js');

const routes = [
    { path: '/', component: Login },
    { path: '/admin', component: Admin, beforeEnter: checkIsAdmin,
        children: [
            {
                path: '/maestros',
                component: crudDocentes
            },
            {
                path: '/diccionario_admin',
                component: crudDiccionario
            }
        ]},
    { path: '/docente', component: Docente, beforeEnter: checkIsDocente,
        children: [
            {
                path: '/diccionario_docente',
                component: crudDiccionario
            },
            {
                path: '/grupos',
                component: crudGrupos
            },
            {
                path: '/add_lectura/:grupo_id',
                component: addLectura
            }
        ]
    } 
  ]


const router = new VueRouter({
    routes,
})

window.Store = store; // <- esta bien loco esto alv
const app = new Vue({
    router,
    store
}).$mount('#app')


// GUARDS

function checkIsAdmin(to, from, next){
    
    if(store.getters.usuario.rol === "admin"){
        next();
    }else{
        next('/');
    }
}
function checkIsDocente(to, from, next){
    if(store.getters.usuario.rol === "maestro"){
        next();
    }else{
        next('/');
    }
}