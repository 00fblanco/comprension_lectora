module.exports = {
    SET_USUARIO(state, usuario){
        state.usuario = usuario;
    },
    GET_DOCENTES(state, docentes){
        state.docentes = docentes;
    },
    DESTROY_ITEM_DOCENTE(state, persona_id){
        state.docentes = state.docentes.filter(item => item.persona_id!==persona_id)
    },
    GET_DICCIONARIO(state, diccionario){
        state.diccionario = diccionario
    },
    GET_GRUPOS(state, grupos){
        state.grupos = grupos
    },
    DESTROY_GRUPO(state, id){
        state.grupos = state.grupos.filter(item => item.grupo.id!==id)
    },
    GET_GRUPO_ALUMNOS(state, alumnos){
        state.grupo_alumnos = alumnos;
    }
}