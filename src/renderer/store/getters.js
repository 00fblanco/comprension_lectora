module.exports = {

    usuario: state => {
        return state.usuario;
    },
    docentes: state => {
        return state.docentes;
    },
    diccionario: state => {
        return state.diccionario
    },
    grupos: state => {
        return state.grupos
    },
    grupo_alumnos: state => {
        return state.grupo_alumnos
    }
      
}