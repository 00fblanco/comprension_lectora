
const store = new Vuex.Store({
    state: {
        usuario: {},
        docentes: [],
        diccionario: [],
        grupos: [],
        grupo_alumnos: []
    }, 
    mutations: require('./mutations.js'),
    actions: require('./actions.js'),
    getters: require('./getters.js')

});

module.exports = store;

