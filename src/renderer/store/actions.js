module.exports = {
    login(context, usuario){
        context.commit('SET_USUARIO', usuario);
    },
    getDocentes(context){
        fetch('api://docente')
            .then(resp => resp.json())
            .then( data => context.commit('GET_DOCENTES', data.rows))
    },
    destroyDocente(context, persona_id){
        fetch(`api://docente/${persona_id}`, {method: 'delete'})
            .then(resp => resp.json())
            .then(data => {
                if(data.status === 'ok'){
                    context.commit('DESTROY_ITEM_DOCENTE', persona_id)
                }
            })
    },
    getDiccionario(context){
        fetch('api://diccionario')
            .then(resp => resp.json())
            .then(data => context.commit('GET_DICCIONARIO', data.rows))
    },
    getGrupos(context, persona_id){
        fetch('api://grupo/'+persona_id)
            .then(resp => resp.json())
            .then(data => context.commit('GET_GRUPOS', data.rows))
    },
    destroyGrupo(context, id){
        fetch(`api://grupo/${id}`, {method: 'delete'})
            .then(resp => resp.json())
            .then(data => {
                if(data.status === 'ok'){
                    context.commit('DESTROY_GRUPO', id)
                }
            })
    },
    getGrupoAlumnos(contex, id){
        fetch(`api://grupo/alumnos/${id}`)
            .then(resp => resp.json())
            .then(data => {
                if(data.status === "ok"){
                    contex.commit('GET_GRUPO_ALUMNOS', data.rows);
                }
            })
    }
}